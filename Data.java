import java.math.*;
import java.text.DecimalFormat;

class Data {
    private BigDecimal value;
    private BigDecimal uncertainty;
    private int sigFigs;
    private int decPlaces;
    
    Data(String value, String uncertainty) {
        this.value = new BigDecimal(value);
        this.uncertainty = new BigDecimal(uncertainty);
        decPlaces = (this.uncertainty.compareTo(BigDecimal.ZERO) == 0) ? 20 : getDecPlaces(value);
        sigFigs = decToSig(value, decPlaces);
        //System.out.println(this.value + " " + this.uncertainty + " " + decPlaces + " " + sigFigs);
    }
    Data(BigDecimal value, BigDecimal uncertainty, int sigFigs, int decPlaces) {
        this.value = value;
        this.uncertainty = uncertainty;
        this.sigFigs = sigFigs;
        this.decPlaces = decPlaces;
    }

    @Override
    public String toString() {
        //System.out.println(this.value + " " + this.uncertainty + " " + decPlaces + " " + sigFigs);
        MathContext m1 = new MathContext(sigFigs);
        BigDecimal uncertaintyRound = (uncertainty.compareTo(BigDecimal.ZERO) == 0)? uncertainty : new BigDecimal("1").scaleByPowerOfTen(-decPlaces).max(uncertainty);
        int uncertaintySig = decToSig(uncertaintyRound.toPlainString(), decPlaces);
        ////System.out.println(value.toPlainString() + " dec places: " + decPlaces);
        MathContext m2 = new MathContext(uncertaintySig);
        DecimalFormat format = new DecimalFormat();
        if (uncertainty.compareTo(BigDecimal.ZERO) != 0){
            format.setMinimumFractionDigits(decPlaces);
        }
        String s = format.format(value.round(m1)) + " & " + format.format(uncertaintyRound.round(m2));
        return s;
    }

    public static int getDecPlaces(String s) {
        int period = s.indexOf(".");
        if (period == -1) {
            int count = 0;
            int i = s.length()-1;
            while (i >= 0 && s.charAt(i) == '0') {
                count--;
                i--;
            }
            return count;
        }
        else {
            return s.length() - 1 - period;
        }
    }

    public static int decToSig(String value, int decPlaces) {
        String v = value.replaceAll("^[-0]+", "");
        int period = v.indexOf(".");
        if (period == -1) {
            return v.length() + decPlaces;
        }
        else {
            v = v.replaceAll("[.]", "");
            //System.out.println(v);
            int i = 0;
            while (i < v.length() && v.charAt(i) == '0') {
                i++;
            }
            int offset = period - i;
            //System.out.println(period + " " + i);
            return offset + decPlaces;
        }

    }

    public static int sigToDec(String value, int sigFigs) {
        String v = value.replaceAll("^[-0]+", "");
        int period = v.indexOf(".");
        if (period == -1) {
            return sigFigs - v.length();
        }
        else {
            v = v.replaceAll("[.]", "");
            //System.out.println(v);
            int i = 0;
            while (i < v.length() && v.charAt(i) == '0') {
                i++;
            }
            int offset = period - i;
            //System.out.println(period + " " + i);
            return sigFigs - offset;
        }
    }

    public static Data add(Data a, Data b) {
        BigDecimal sum = a.value.add(b.value);
        BigDecimal uncertainty = a.uncertainty.add(b.uncertainty);
        int decPlaces = Math.min(a.decPlaces, b.decPlaces);
        int sigFigs = decToSig(sum.toPlainString(), decPlaces);
        return new Data(sum, uncertainty, sigFigs, decPlaces);
    }

    public static Data sub(Data a, Data b) {
        BigDecimal sum = a.value.subtract(b.value);
        BigDecimal uncertainty = a.uncertainty.add(b.uncertainty);
        int decPlaces = Math.min(a.decPlaces, b.decPlaces);
        int sigFigs = decToSig(sum.toPlainString(), decPlaces);
        return new Data(sum, uncertainty, sigFigs, decPlaces);
    }

    public static Data multiply(Data a, Data b) {
        BigDecimal product = a.value.multiply(b.value);
        BigDecimal uncertainty = a.uncertainty.multiply(b.value.abs()).add(b.uncertainty.multiply(a.value.abs()));
        int sigFigs = Math.min(a.sigFigs, b.sigFigs);
        int decPlaces = sigToDec(product.toPlainString(), sigFigs);
        return new Data(product, uncertainty, sigFigs, decPlaces);
    }

    public static Data divide(Data a, Data b) {
        MathContext m = new MathContext(20);
        BigDecimal quotient = a.value.divide(b.value, m);
        BigDecimal percentA = a.uncertainty.divide(a.value.abs(), m);
        BigDecimal percentB = b.uncertainty.divide(b.value.abs(), m);
        BigDecimal uncertainty = percentA.add(percentB).multiply(quotient.abs());
        int sigFigs = Math.min(a.sigFigs, b.sigFigs);
        int decPlaces = sigToDec(quotient.toPlainString(), sigFigs);
        return new Data(quotient, uncertainty, sigFigs, decPlaces);
    }

    public BigDecimal getValue(){
        return value;
    }
    public BigDecimal getUncertainty(){
        return uncertainty;
    }
    public int getSigFigs(){
        return sigFigs;
    }
    public int getDecimalPoints(){
        return decPlaces;
    }
}