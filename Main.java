import java.util.*;
import java.lang.Math.*;
import java.math.BigDecimal;
import java.math.MathContext;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
class Main {


    public static void main(String args[]) {
        /*
        Data a = new Data("-13.35", "1");
        Data b = new Data("-10.", "0");
        System.out.println("(" + a + ") / (" + b + ")");
        System.out.println (Data.multiply(a, b));
        */
        String intro = "\nError Propagation Calculator\n"
        + "By Warren Sunada-Wong\n"
        + "Recursive descent parser from https://stackoverflow.com/questions/3422673/how-to-evaluate-a-math-expression-given-in-string-form\n"
        + "-----------------------------------------------------\n"
        + "Enter a math expression with uncertainties. This calculator will propagate errors and handle significant figures for you!\t\n\n"
        + "Supported operations:\n"
        + "   +   Addition\n"
        + "   -   Subtraction\n"
        + "   *   Multiplication\n"
        + "   /   Division\n"
        + "   ()  Parentheses\n"
        + "   &  Uncertainty\n\n"
        + "Note: There may be mistakes in the code -- do not rely solely on this calculator.\t\n"
        + "Order of operations apply. Uncertainty (&) takes highest precedence. Spaces are optional.\n"
        + "If no uncertainty is specified, the default value is zero.\n\n"
        + "Example: (10.35 & 0.04) + (13.1 & 0.3) = 23.5 & 0.3\n"
        + "Example: 4 * 4.2 & 0.2 = 17 & 1\n"
        + "----------------------------------------------------\n";
        //System.out.println(intro);
        //JOptionPane.showMessageDialog(null, intro, "Error Propagation", JOptionPane.PLAIN_MESSAGE );
        Scanner s = new Scanner(System.in);
        //System.out.println(Data.decToSig("0.0152671755725190839694", 2));
        while (true) {
            //System.out.println("Enter an expression (Press q to quit):");
            //String input = s.nextLine();
            String input = JOptionPane.showInputDialog(intro + "Enter an expression:");
            if (input.equals("q")) {
                //s.close();
                System.exit(0);
                return;
            }
            Eval e = new Eval(input);
            //System.out.println(e.eval() + "\n");
            JOptionPane.showMessageDialog(null,intro + "Enter an expression:\n" + input + "\nAnswer: " + e.eval(), "Answer", JOptionPane.PLAIN_MESSAGE );
        }
    }
}