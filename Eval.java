import java.math.BigDecimal;

// Recursive descent parser from https://stackoverflow.com/questions/3422673/how-to-evaluate-a-math-expression-given-in-string-form
class Eval {
    String s;
    int i;
    int ch;

    public void nextChar() {
        i++;
        ch = (i < s.length()) ? s.charAt(i) : -1;
    }

    public boolean nextIs(char c) {
        while (ch == ' ') nextChar();
        //System.out.println("nextIs: " + c + "? " + (char)ch);
        if (ch == c) {
            nextChar();
            return true;
        }
        else {
            return false;
        }
    }

    public String nextDecimal() {
        int pos = i;
        while (ch >= '0' && ch <= '9' || ch == '.') {
            nextChar();
        }
        return s.substring(pos, i);
    }

    public Data nextFactor() {
        if (nextIs('-')) return Data.multiply(nextFactor(), new Data("-1", "0"));
        Data x;
        if (nextIs('(')) {
            x = nextExpression();
            nextIs(')');
        }
        else if (ch >= '0' && ch <= '9' || ch == '.') {
            String value = nextDecimal();
            if (nextIs('&')) {
                nextIs(' ');
                String uncertainty = nextDecimal();
                x = new Data(value, uncertainty);
            }
            else {
                x = new Data(value, "0");
            }
        }
        else {
            throw new RuntimeException("No digit!");
        }
        return x;
    }

    public Data nextTerm() {
        Data x = nextFactor();
        while (true) {
            if (nextIs('*')) {
                x = Data.multiply(x, nextFactor());
            }
            else if (nextIs('/')) {
                x = Data.divide(x, nextFactor());
            }
            else {
                return x;
            }
        }
    }

    public Data nextExpression() {
        Data x = nextTerm();
        while (true) {
            if (nextIs('+')) {
                x = Data.add(x, nextTerm());
            }
            else if (nextIs('-')) {
                x = Data.sub(x, nextTerm());
            }
            else {
                return x;
            }
        }
    }

    public Data eval() {
        nextChar();
        return nextExpression();
    }

    Eval(String s) {
        i = -1;
        this.s = s;
    }

}